const AWS = require("aws-sdk");
const documentClient = new AWS.DynamoDB.DocumentClient({
    region: process.env.REGION,
    convertEmptyValues: true,
    ...(process.env.JEST_WORKER_ID && {
        endpoint: process.env.DYNAMO_LOCAL || 'localhost:8000',
        sslEnabled: false,
        region: 'local-env',
    })
});

module.exports = {
    documentClient: documentClient,
    update: async (tableName, tableKey, values) => {
        const updateExpression = "SET " + Object.keys(values).map(k => `#${k} = :${k}`).join(", ");
        const expressionAttributeNames = {};
        for (let k in values) {
            expressionAttributeNames[`#${k}`] = k;
        }
        const expressionAttributeValues = {};
        for (let k in values) {
            expressionAttributeValues[`:${k}`] = values[k];
        }

        let response = await documentClient.update({
            TableName: tableName,
            Key: tableKey,
            UpdateExpression: updateExpression,
            ExpressionAttributeNames: expressionAttributeNames,
            ExpressionAttributeValues: expressionAttributeValues,
            ReturnValues: "ALL_NEW"
        }).promise();

        return response['Attributes'];
    },

    get: async (tableName, tableKey) => {
        let response = await documentClient.get({
            TableName: tableName,
            Key: tableKey,
        }).promise();

        return response['Item'] || null;
    },

    put: async (tableName, item) => {
        let response = await documentClient.put({
            TableName: tableName,
            Item: item,
            ConditionExpression: 'attribute_not_exists(id)'
        }).promise();

        return item;
    },

    increment: async (tableName, tableKey, values) => {
        const updateExpression = "SET " + Object.keys(values).map(k => `#${k} = if_not_exists(#${k}, :start) + :${k}`).join(", ");
        const expressionAttributeNames = {};
        for (let k in values) {
            expressionAttributeNames[`#${k}`] = k;
        }
        const expressionAttributeValues = {};
        for (let k in values) {
            expressionAttributeValues[`:${k}`] = values[k];
        }
        expressionAttributeValues[':start'] = 0;

        let response = await documentClient.update({
            TableName: tableName,
            Key: tableKey,
            UpdateExpression: updateExpression,
            ExpressionAttributeNames: expressionAttributeNames,
            ExpressionAttributeValues: expressionAttributeValues,
            ReturnValues: "ALL_NEW"
        }).promise();

        return response['Attributes'];
    },
}